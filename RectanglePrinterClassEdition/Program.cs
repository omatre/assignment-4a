﻿using System;

namespace RectanglePrinterClassEdition
{
    class Program
    {
        static void Main(string[] args)
        {
            char selectKey = PromptUserSquareOrRect();

            if (selectKey == 's')
            {
                PrintSquare();
            }
            else if (selectKey == 'r')
            {
                PrintRectangle();
            }
        }

        private static void PrintRectangle()
        {
            int sideOne = InputLengthOf("first side");
            int sideTwo = InputLengthOf("second side");

            if (sideOne == 0 || sideTwo == 0)
            {
                Console.WriteLine("One or both of sides zero or invalid value.");
            }
            else if (sideOne < 0 || sideTwo < 0)
            {
                Console.WriteLine("One or both sides negative.");
            }

            PrintGeneralRectangle(sideOne, sideTwo);
        }

        private static void PrintSquare()
        {
            int side = InputLengthOf("sides");

            if (side == 0)
            {
                Console.WriteLine("Side zero or invalid value.");
            }
            else if (side < 0)
            {
                Console.WriteLine("Side negative value.");
            }

            PrintGeneralRectangle(side, side);
        }

        private static void PrintGeneralRectangle(int side1, int side2)
        {
            for (int i = 0; i < side1; i++)
            {
                for (int j = 0; j < side2; j++)
                {
                    if (i == 0 || i == (side1 - 1) || j == 0 || j == (side2 - 1))
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }

        private static int InputLengthOf(string v)
        {
            Console.WriteLine($"Input length of { v }...");
            int.TryParse(Console.ReadLine(), out int length);

            return length;
        }

        private static char PromptUserSquareOrRect()
        {
            Console.WriteLine("Do you want to create a square or a rectangle? (S/R)");

            try
            {
                char key = Console.ReadKey().KeyChar;

                if (key == 's' || key == 'S')
                {
                    return 's';
                }
                else if (key == 'r' || key == 'R')
                {
                    return 'r';
                }
                else
                {
                    throw new InvalidChoiceException($"{ key } is an invalid choice, we choose square for ya...");
                }
            }
            catch (InvalidChoiceException ex)
            {
                Console.WriteLine(ex.Message);
                return 's';
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
